# README #

Create a basic directory structure for new Python projects.


* docs
* LICENSE
* project_src
    * controller
          * file.py
          * __init__.py
          *  main.py
     * model
          * file.py
          * __init__.py
          *  main.py
     * view
          * file.py
          * __init__.py
          *  main.py
     * README.md
     * requirements.txt
     * resources
          * data
          * images
     * setup.py
     * test
          * __init__.py
          * test_main.py
     * TODO.md