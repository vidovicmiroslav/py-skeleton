#!/bin/bash

# -------------------------------------------------------
# Info:
# 	Miroslav Vidovic
# 	pyproject_structure.sh
# 	11.03.2016.-12:18:50
# -------------------------------------------------------
# Description:
#  Bash scirpt to create a directory structure for python
#  projects.
# Usage:
#
# -------------------------------------------------------
# Script:

project_name=$1

check_project_name(){
  if [ -z $project_name ]; then
    project_name="Python_project"
  fi
}


# Create directories in the root directory
create_main_structure(){
  mkdir -p $project_name/{docs,project_src,tests,resources}
}

# Create files in the project root directory
create_files_in_root(){
  file_array=(LICENSE README.md TODO.md requirements.txt setup.py .gitignore main.py)
  for file in "${file_array[@]}"
  do
    touch $project_name/$file
  done
}

# Create directories in resources
create_resources_structure(){
  dir_array=(data images)
  for dir in "${dir_array[@]}"
  do
    mkdir -p $project_name/resources/$dir
  done
}

# Create files in the project/docs directory
# TODO: Check sphinx and add to this function
create_files_in_docs(){
  return
}

# Create directories in project_src
crate_project_src_structure(){
  dir_array=(model view controller)
  for dir in "${dir_array[@]}"
  do
    mkdir -p $project_name/project_src/$dir
  done
}

# Create files in the project/project_name directory
create_files_in_project_src(){
  file_array=(__init__.py file.py main.py)
  for file in "${file_array[@]}"
  do
    touch $project_name/project_src/model/$file
    touch $project_name/project_src/view/$file
    touch $project_name/project_src/controller/$file
  done
}

# Create files in the project/tests directory
create_files_in_tests(){
  file_array=(__init__.py test_main.py)
  for file in "${file_array[@]}"
  do
    touch $project_name/tests/$file
  done
}

populate_gitignore(){
  cat << EOF
  # Byte-compiled / optimized / DLL files
  __pycache__/
  *.py[cod]
  *$py.class
  #
  # PyCharm stuff
  .idea
EOF
}

# Put a signature in the README.md file
info () {
cat << "EOF"
    _____       _   _
   |  __ \     | | | |
   | |__) |   _| |_| |__   ___  _ __
   |  ___/ | | | __| '_ \ / _ \| '_ \
   | |   | |_| | |_| | | | (_) | | | |
   |_|    \__, |\__|_| |_|\___/|_| |_|
           __/ |
          |___/

EOF
}

# Main program
main_program(){
  check_project_name
  create_main_structure
  create_files_in_root
  populate_gitignore > $project_name/.gitignore
  crate_project_src_structure
  create_resources_structure
  create_files_in_docs
  create_files_in_project_src
  create_files_in_tests
  info
}

main_program

exit 0
